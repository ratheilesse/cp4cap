
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import oscar.cp._

class TestArborescence extends FunSuite with ShouldMatchers {

  def ArboDecomp0(cp: CPSolver, preds: Array[CPIntVar], tab: Array[Array[Int]], root: Int, H: CPIntVar) = {

    val n = tab.length
    val xij = Array.fill(n, n)(CPIntVar(0 to 1)(cp))


    for (w <- 2 to n - 1) {
      val s = combinations((1 until n).toList, w)
      for (k <- s) {
        //indegree requirements
        cp.add(sum(for (i <- 0 to n - 1; if (!k.contains(i))) yield i, for (j <- k) yield j) { case (i, j) => xij(i)(j) } >= 1)
      }
    }

    cp.add(sum(n, n) { case (i, j) => xij(i)(j) } === n - 1)
    cp.add(sum(0 until n, 1 until n) { case (i, j) => xij(i)(j) * tab(i)(j) } <= H)

    for (j <- 1 until n) {
      cp.add(sum(0 until n)(i => xij(i)(j)) === 1)
    }

    for (i <- 0 until n) {
      cp.add((xij(i)(0)) === 0)
    }
    for (i <- 0 until n; j <- 0 until n) {
      cp.add((xij(i)(j) ?=== 1) ==> (preds(j) ?=== i))
    }
    cp.add(preds(root) === root)

  }

  def ArboDecomp1(cp: CPSolver, preds: Array[CPIntVar], tab: Array[Array[Int]], root: Int, H: CPIntVar) = {

    val n = tab.length
    val xij = Array.fill(n, n)(CPIntVar(0 to 1)(cp))

    cp.add(new Arborescence(preds, root))

    for (i <- 0 until n; j <- 0 until n) {
      cp.add((preds(j) ?=== i) ==> (xij(i)(j) ?=== 1))
    }
    cp.add(sum(0 until n, 1 until n) { case (i, j) => xij(i)(j) * tab(i)(j) } <= H)

  }

  def nbSol(domX: Array[Set[Int]], tab: Array[Array[Int]], root: Int, domH: Set[Int], decomp: Int = 0): (Int, Int, Int) = {
    val cp = CPSolver()
    var nbSol = 0
    val preds = Array.tabulate(domX.size)(i => CPIntVar(domX(i))(cp))
    val H = CPIntVar(domH)(cp)

    if (decomp == 0) {
      ArboDecomp0(cp, preds, tab, root, H)
    } else if (decomp == 1) {
      ArboDecomp1(cp, preds, tab, root, H)
    } else if (decomp == 2) {
      cp.add(new MinArborescence(preds, tab, root, H, false))
    } else {
      cp.add(new MinArborescence(preds, tab, root, H, true))
    }

    cp.search {
      binaryStatic(preds)
    } onSolution {
      nbSol += 1
    }

    val stat = cp.start()
    (nbSol, stat.nFails, stat.nNodes)
  }

  test("Arbo1") {
    val x1 = (0 to 4).toSet
    val x2 = (0 to 4).toSet
    val x3 = (0 to 4).toSet
    val x4 = (0 to 4).toSet
    val x5 = (0 to 4).toSet
    val domX = Array(x1, x2, x3, x4, x5)
    val domH = (0 to 38).toSet
    val tab = Array(Array(9999, 10, 2, 10, 9999), Array(2, 9999, 1, 9999, 9999),
      Array(9999, 9999, 9999, 4, 9999), Array(9999, 9999, 9999, 9999, 2),
      Array(9999, 2, 9999, 9999, 9999))

    var (nSol0, nSol1, nSol2, nSol3) = (0, 0, 0, 0)
    var (bkt0, bkt1, bkt2, bkt3) = (0, 0, 0, 0)
    var (nNode0, nNode1, nNode2, nNode3) = (0, 0, 0, 0)

    val t0 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 0)
      nSol0 = a
      bkt0 = b
      nNode0 = c
    }

    val t1 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 1)
      nSol1 = a
      bkt1 = b
      nNode1 = c
    }
    val t2 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 2)
      nSol2 = a
      bkt2 = b
      nNode2 = c
    }
    val t3 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 3)
      nSol3 = a
      bkt3 = b
      nNode3 = c
    }
    nSol0 should equal(nSol1)
    nSol1 should equal(nSol2)
    nSol2 should equal(nSol3)

  }

  test("Arbo2") {
    val x1 = (0 to 4).toSet
    val x2 = (0 to 4).toSet
    val x3 = (0 to 4).toSet
    val x4 = (0 to 4).toSet
    val x5 = (0 to 4).toSet
    val domX = Array(x1, x2, x3, x4, x5)
    val domH = (0 to 60).toSet
    val tab = Array(Array(9999, 14, 0, 11, 5), Array(21, 9999, 0, 24, 15),
      Array(14, 23, 9999, 19, 4), Array(12, 15, 24, 9999, 4),
      Array(19, 11, 12, 13, 9999))

    var (nSol0, nSol1, nSol2, nSol3) = (0, 0, 0, 0)
    var (bkt0, bkt1, bkt2, bkt3) = (0, 0, 0, 0)
    var (nNode0, nNode1, nNode2, nNode3) = (0, 0, 0, 0)

    val t0 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 0)
      nSol0 = a
      bkt0 = b
      nNode0 = c
    }

    val t1 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 1)
      nSol1 = a
      bkt1 = b
      nNode1 = c
    }
    val t2 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 2)
      nSol2 = a
      bkt2 = b
      nNode2 = c
    }
    val t3 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 3)
      nSol3 = a
      bkt3 = b
      nNode3 = c
    }
    nSol0 should equal(nSol1)
    nSol1 should equal(nSol2)
    nSol2 should equal(nSol3)

  }

  test("Arbo3") {
    val x1 = (0 to 4).toSet
    val x2 = (0 to 4).toSet
    val x3 = (0 to 4).toSet
    val x4 = (0 to 4).toSet
    val x5 = (0 to 4).toSet
    val domX = Array(x1, x2, x3, x4, x5)
    val domH = (0 to 60).toSet
    val tab = Array(Array(9999, 2, 20, 5, 7), Array(17, 9999, 15, 0, 22),
      Array(7, 15, 9999, 26, 2), Array(28, 29, 28, 9999, 7),
      Array(21, 7, 16, 10, 9999))

    var (nSol0, nSol1, nSol2, nSol3) = (0, 0, 0, 0)
    var (bkt0, bkt1, bkt2, bkt3) = (0, 0, 0, 0)
    var (nNode0, nNode1, nNode2, nNode3) = (0, 0, 0, 0)

    val t0 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 0)
      nSol0 = a
      bkt0 = b
      nNode0 = c
    }

    val t1 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 1)
      nSol1 = a
      bkt1 = b
      nNode1 = c
    }
    val t2 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 2)
      nSol2 = a
      bkt2 = b
      nNode2 = c
    }
    val t3 = oscar.util.time {
      val (a, b, c) = nbSol(domX, tab, 0, domH, 3)
      nSol3 = a
      bkt3 = b
      nNode3 = c
    }
    nSol0 should equal(nSol1)
    nSol1 should equal(nSol2)
    nSol2 should equal(nSol3)

  }


  val rand = new scala.util.Random(1)

  //  def randomDom(size: Int) = (1 to (rand.nextInt(size) + 4)).toSet
  def randomDom(size: Int) = (0 to size - 1).toSet


  test("Arbo5") {
    var nbWins, nbLosts = 0
    for (i <- 1 to 1000) {

      val nbVars = 5
      val domVars = Array.fill(nbVars)(randomDom(size = nbVars))
      val domH = (0 to rand.nextInt(nbVars) + 50).toSet

      val tab: Array[Array[Int]] = Array.ofDim(nbVars, nbVars)
      for (i <- 0 until nbVars) {
        for (j <- 0 until nbVars) {
          if (i == j) {
            tab(i)(j) = 9999
          } else {
            tab(i)(j) = rand.nextInt(10)
          }
          if (rand.nextInt(100) > 90) tab(i)(j) = 9999
        }
      }

      var (nSol0, nSol1, nSol2, nSol3) = (0, 0, 0, 0)
      var (bkt0, bkt1, bkt2, bkt3) = (0, 0, 0, 0)
      var (nNode0, nNode1, nNode2, nNode3) = (0, 0, 0, 0)

      val t0 = oscar.util.time {
        val (a, b, c) = nbSol(domVars, tab, 0, domH, 0)
        nSol0 = a
        bkt0 = b
        nNode0 = c
      }

      val t1 = oscar.util.time {
        val (a, b, c) = nbSol(domVars, tab, 0, domH, 1)
        nSol1 = a
        bkt1 = b
        nNode1 = c
      }
      val t2 = oscar.util.time {
        val (a, b, c) = nbSol(domVars, tab, 0, domH, 2)
        nSol2 = a
        bkt2 = b
        nNode2 = c
      }
      val t3 = oscar.util.time {
        val (a, b, c) = nbSol(domVars, tab, 0, domH, 3)
        nSol3 = a
        bkt3 = b
        nNode3 = c
      }
      nSol0 should equal(nSol1)
      nSol1 should equal(nSol2)
      nSol2 should equal(nSol3)
    }
  }

  def combinations[A](s: List[A], k: Int): List[List[A]] =
    if (k > s.length) Nil
    else if (k == 1) s.map(List(_))
    else combinations(s.tail, k - 1).map(s.head :: _) ::: combinations(s.tail, k)

}