/*******************************************************************************
  * OscaR is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Lesser General Public License as published by
  * the Free Software Foundation, either version 2.1 of the License, or
  * (at your option) any later version.
  *
  * OscaR is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU Lesser General Public License  for more details.
  *
  * You should have received a copy of the GNU Lesser General Public License along with OscaR.
  * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
  ******************************************************************************/

import oscar.algo.reversible.{ReversibleInt, ReversibleSparseSetJava}
import oscar.cp.core.CPOutcome._
import oscar.cp.core.variables.CPIntVar
import oscar.cp.core.{CPOutcome, CPPropagStrength, Constraint}

/**
  * Arborescence Constraint

  * @param preds
  * @see CPPropagStrength
  * @author Pierre Schaus pschaus@gmail.com
  * @author Ratheil Houndji  ratheilesse@gmail.com
  */
final class Arborescence(preds: Array[CPIntVar], root: Int) extends Constraint(preds(0).store, "BasicArborescence") {

  /*******************************************************************************
    * OscaR is free software: you can redistribute it and/or modify
    * it under the terms of the GNU Lesser General Public License as published by
    * the Free Software Foundation, either version 2.1 of the License, or
    * (at your option) any later version.
    *
    * OscaR is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU Lesser General Public License  for more details.
    *
    * You should have received a copy of the GNU Lesser General Public License along with OscaR.
    * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
    ******************************************************************************/

  priorityL2 = 3

  require(preds.length > 0, "no variable.")

  private[this] val n = preds.size
  private[this] val localRoot = Array.tabulate(n)(i => new ReversibleInt(s, i))
  private[this] val leafNodes = Array.tabulate(n)(i => new ReversibleSparseSetJava(s,0,n-1,true))


  final override def setup(l: CPPropagStrength): CPOutcome = {
    for (i <- 0 until n; if i != root) {
      leafNodes(i).insert(i)
    }
    // Create the self loop on the root
    if (preds(root).assign(root) == Failure) return Failure
    // Attach callback on bind events
    for (i <- 0 until n; if i != root) {
      if (preds(i).removeValue(i) == Failure) return Failure
      else if (preds(i).isBound && valBindIdx(preds(i), i) == Failure) return Failure
      else preds(i).filterWhenBind() {
        bind(i)
      }
    }
    Suspend
  }

  private[this] val values = Array.ofDim[Int](n)

  private def bind(i: Int): CPOutcome = {

    val j = preds(i).min

    val newLocalRoot = localRoot(j)

    // we could remove i from leafNodes(newLocalRoot) but we can't with a reversible sparse-set

    var s = leafNodes(i).fillArray(values)
    while (s > 0) {
      s -= 1
      val l = values(s)
      localRoot(l).value = newLocalRoot
      leafNodes(newLocalRoot).insert(l)
      if (preds(newLocalRoot).removeValue(l) == Failure) return Failure
    }
    return Suspend

  }
}