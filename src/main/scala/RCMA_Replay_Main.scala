import java.io.File

import oscar.algo.search.DFSLinearizer
import oscar.cp._
import oscar.util._
import oscar.cp.constraints.WeightedSum

import scala.io.Source


/**
  * Resource constrained Minimum Weight Arborescence Problem (RMWA).
  * RMWA consists to find an Minimum Weight Arborescence under the resource constraints for each vertex $i \in V$:
  * $\sum_{(i, j) \in \delta^+_i}{a_{i,j} * x_{i,j} \le b_i}$
  * in which $\delta^+_i$ is the set of outgoing edges from $i$,
  * $a_{i,j}$ is the amount of resource uses by the edge $(i,j)$ and
  * $b_i$ is the resource available at vertex $i$.
  *
  * @author Ratheil Houndji, ratheilesse@gmail.com
  */


case class Param(inputFile: File = new File("."),
                 timeout: Int = 30,
                 model: Int = 0)

object RCMA_Replay_Main extends App {

  val parser = new scopt.OptionParser[Param]("CP4CAP") {
    head("CP4CAP", "1.0")

    arg[File]("<Input File>") action { (x, c) =>
      c.copy(inputFile = x)
    } validate { x =>
      if (x.exists()) success else failure("<Input File> does not exist")
    } text ("the input instance")
    opt[Int]('t', "<the time limit>") action { (x, c) =>
      c.copy(timeout = x)
    } validate { x =>
      if (x > 0) success else failure("the timeout must be > 0")
    }
    opt[Int]('m', "<the model to use>") action { (x, c) =>
      c.copy(model = x)
    } validate { x =>
      if (x > 0 && x <= 4) success else failure("the model number is in [1,...,3]")
    }
  }

  parser.parse(args, Param()) match {
    case Some(config) =>
      val root = 0
      implicit val cp = CPSolver()
      cp.silent = true
      val src = config.inputFile
      val lines = Source.fromFile(src).getLines.reduceLeft(_ + " " + _)
      val vals = lines.split("[ ,\t]").toList.filterNot(_ == "").map(_.toInt)
      var index = 0

      def next() = {
        index += 1
        vals(index - 1)
      }

      val n = next()
      val tab: Array[Array[Int]] = Array.ofDim(n, n)
      for (i <- 0 until n) {
        for (j <- 0 until n) {
          tab(i)(j) = next()
        }
      }

      val a: Array[Array[Int]] = Array.ofDim(n, n)
      for (i <- 0 until n) {
        for (j <- 0 until n) {
          a(i)(j) = next()
        }
      }

      val uB: Array[Int] = Array.ofDim(n)
      for (i <- 0 until n) {
        uB(i) = next()
      }

      for (i <- 0 until n) {
        for (j <- 0 until n) {
          if (a(i)(j) > uB(i)) tab(i)(j) = 9999
        }
      }

      val pred = Array.tabulate(n)(i => CPIntVar(0 until n)(cp))
      val z = CPIntVar(0 to 10000)(cp)
      val b = Array.tabulate(n)(i => CPIntVar(0 to uB(i))(cp))
      val x = Array.fill(n, n)(CPBoolVar()(cp))

      var best = Array.fill(n)(0)
      var bestCost = Integer.MAX_VALUE

      //for ressource constrained minimum weight arborescence problem
      for (i <- 0 until n) {
        //cp.add(sum(0 until n)(j => xij(i)(j) * a(i)(j)) <= b(i))
        cp.add(binaryKnapsack(x(i), a(i), b(i)), Weak)
      }
      ////////////////////////////////////
      cp.add(sum(n, n) { case (i, j) => x(i)(j) } === n - 1)

      cp.add(new WeightedSum(tab.flatten, x.flatten, z), Weak)

      for (j <- 1 until n) {
        cp.add(sum(0 until n)(i => x(i)(j)) === 1)
      }
      for (i <- 0 until n) {
        cp.add((x(i)(0)) === 0)
      }

      //Channeling constraints
      for (i <- 0 until n; j <- 0 until n) {
        cp.add((x(i)(j) ?=== 1) ==> (pred(j) ?=== i))
      }
      cp.add(pred(root) === root)
      ////////////////////////////////////

      cp.add(new Arborescence(pred, root), Weak)

      cp.minimize(z)

      cp.onSolution {
        //println(pred.mkString(","))
        best = pred.map(_.value)
        bestCost = z.value
      }

      //Search
      cp.search {
        //binaryStatic(pred)
        if(config.model == 0) binaryFirstFail(pred)
        else conflictOrderingSearch(pred, i => pred(i).size, i => selectMin(0 until n)(pred(i).hasValue(_))(tab(_)(i)).get)
      }


      if(config.model == 0) {
        val linearizer = new DFSLinearizer()
        // Record the search tree with a basic decomposition
        val statsInit = startSubjectTo(timeLimit = config.timeout, searchListener = linearizer) {
        }
        println("linearization stats:")
        println(statsInit)

        // Replay
        //cp.objective

        cp.obj(z).relax
        val statsArbo = cp.replaySubjectTo(linearizer, pred) {
        }
        println("arbo")
        println(statsArbo)

        // Replay
        //cp.objective
        cp.obj(z).relax
        val statsArboWithlB = cp.replaySubjectTo(linearizer, pred) {
          cp.add(new MinArborescenceWithoutReducedCosts(pred, tab, root, z, false))
        }
        println("arboWithLB")
        println(statsArboWithlB)

        // Replay with minArbo
        cp.obj(z).relax
        val statsMinArbo = cp.replaySubjectTo(linearizer, pred) {
          cp.add(new MinArborescence(pred, tab, root, z, false))
        }
        println("minArbo")
        println(statsMinArbo)

        // Replay with arboWithIRC
        cp.obj(z).relax
        val statsArboWithIRC = cp.replaySubjectTo(linearizer, pred) {
          cp.add(new MinArborescence(pred, tab, root, z, true))
        }
        println("minArboWithIRC")
        println(statsArboWithIRC)
        println("-------------")
        println("\nfinished")
      } else {
        if (config.model == 2){
          cp.add(new MinArborescenceWithoutReducedCosts(pred, tab, root, z, false))
        } else if(config.model == 3){
          cp.add(new MinArborescence(pred, tab, root, z, false))
        } else if(config.model == 4){
          cp.add(new MinArborescence(pred, tab, root, z, true))
        }
        val stats = cp.start(timeLimit = config.timeout)
        println(stats)
        println("best cost found => " + bestCost)
      }
    case None =>
    //parser.showUsage
  }
}