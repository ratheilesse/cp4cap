import oscar.algo.reversible._
import oscar.cp._
import oscar.cp.core.CPOutcome._
import oscar.cp.core._

/**
  * The minArborescence constraint is defined as: arborescence(preds, w, root, z)
  * in which preds(i) is the predecessor of the vertex i in the arborescence A(G) of the graph G found,
  * w is a cost function on edges of G,
  * root is a vertex and
  * z is an upper bound of the cost of arborescence of G rooted at the vertex r.
  * The constraint holds when there exists an arborescence A(G) rooted at the vertex r with w(A(G)) ≤ K.
  *
  * @param preds   , preds(i) is the predecessor of the vertex i in the arborescenc
  * @param w       , w(i)(j) is the weight of the edge (i,j)
  * @param root    , the root vertex of the arborescence
  * @param z       , the variable $z$ is an upper bound on the cost of the arborescence
  * @param withIRC , boolean variable: withIRC = true for filtering based on
  *                improved reduced costs ; withIRC = false for filtering based on
  *                reduced costs
  *
  *                O(n*n) in which n is the number of vertices
  *
  * @author Ratheil Houndji, ratheilesse@gmail.com
  */

class MinArborescence(val preds: Array[CPIntVar], val w: Array[Array[Int]], val root: Int, val z: CPIntVar, val withIRC: Boolean = true) extends Constraint(preds(0).store, "Arborescence") {

  /*******************************************************************************
    * OscaR is free software: you can redistribute it and/or modify
    * it under the terms of the GNU Lesser General Public License as published by
    * the Free Software Foundation, either version 2.1 of the License, or
    * (at your option) any later version.
    *
    * OscaR is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU Lesser General Public License  for more details.
    *
    * You should have received a copy of the GNU Lesser General Public License along with OscaR.
    * If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
    ******************************************************************************/

  priorityL2 = 0

  val arbo = new ArborWithoutBreakable(w, root, true)
  val n = preds.length

  val M = z.max + 1

  val costMatrix = Array.tabulate(n, n)((i, j) => new ReversibleInt(s, if (!preds(j).hasValue(i)) M else w(i)(j)))

  override def setup(l: CPPropagStrength): CPOutcome = {

    if (preds(root).assign(root) == Failure) return Failure

    //s.post(new Arborescence(preds, root))

    var k = 0
    while (k < n) {
      if (preds(k).updateMax(n - 1) == Failure) {
        return Failure
      }
      k += 1
    }

    k = 0
    while (k < n) {
      if (!preds(k).isBound) {
        preds(k).callValRemoveIdxWhenValueIsRemoved(this, k)
        preds(k).callPropagateWhenDomainChanges(this)
      }
      k += 1
    }
    if (!z.isBound) {
      z.callPropagateWhenBoundsChange(this)
    }

    propagate()
  }

  override def valRemoveIdx(y: CPIntVar, k2: Int, k1: Int): CPOutcome = {
    costMatrix(k1)(k2).value = M
    Suspend
  }

  override def propagate(): CPOutcome = {

    val maxZ = z.max

    val min = arbo.arbred(costMatrix, maxZ)

    if (z.updateMin(min) == Failure) {
      return CPOutcome.Failure
    }

    if (withIRC) arbo.computeImprovedRC()

    val gap = maxZ - min

    var k1 = 0
    while (k1 < n) {
      var k2 = 0
      while (k2 < n) {
        val gradient = if (withIRC) arbo.improvedRC(k1)(k2)
        else arbo.rc(k1)(k2)

        if (k2 != root && gradient > gap) {
          if (preds(k2).removeValue(k1) == Failure) {
            return Failure
          }
        }
        k2 += 1
      }
      k1 += 1
    }

    Suspend
  }
}