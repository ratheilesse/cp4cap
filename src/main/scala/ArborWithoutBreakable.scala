
import java.io.{FileReader, BufferedReader}
import java.io.PrintWriter
import java.io.File

import scala.io.Source
import oscar.algo.reversible._

/**
  * Computation of Minimum Weigh Arborescence and LP reduced costs
  * Based on O(n^2) fortran implementation of
  * (Fischetti and Toth, 1993: AN EFFICIENT ALGORITHM FOR THE MIN-SUM
  * ARBORESCENCE PROBLEM ON COMPLETE DIGRAPHS)
  *
  *
  * @param tab    , tab(i)(j) is the weight of the edge (i,j)
  * @param root   , is a vertex, the root of the Minimum Weight Arborescence
  * @param rcflag , true if LP reduced costs are wanted, false otherwise.
  *
  * @author Ratheil Houndji, ratheilesse@gmail.com
  *
  */
class ArborWithoutBreakable(var tab: Array[Array[Int]], var root: Int, val rcflag: Boolean) {

  val verbose = false

  root += 1
  val n = tab(0).length
  var k1, k2 = 1
  val c = Array.ofDim[Int](n + 1, n + 1)
  val rc = Array.ofDim[Int](n + 1, n + 1)

  val mm = 2 * n
  val pred: Array[Int] = Array.ofDim(mm + 1)
  val stack: Array[Int] = Array.ofDim(mm + 1)
  val sv: Array[Int] = Array.ofDim(n + 1)
  val shadow: Array[Int] = Array.ofDim(n + 1)
  val lnext: Array[Int] = Array.ofDim(mm + 1)
  val lpred: Array[Int] = Array.ofDim(mm + 1)
  val arsel1: Array[Int] = Array.ofDim(mm + 1)
  val arsel2: Array[Int] = Array.ofDim(mm + 1)
  val parent: Array[Int] = Array.ofDim(mm + 1)
  val u: Array[Int] = Array.ofDim(mm + 1)
  val label: Array[Int] = Array.ofDim(mm + 1)
  val line: Array[Int] = Array.ofDim(mm + 1)
  val fson: Array[Int] = Array.ofDim(mm + 1)
  val broth: Array[Int] = Array.ofDim(mm + 1)
  val left: Array[Int] = Array.ofDim(mm + 1)
  val right: Array[Int] = Array.ofDim(mm + 1)
  val node: Array[Int] = Array.ofDim(mm + 1)
  val delet = Array.fill(mm + 1)(false)
  val reduc: Array[Int] = Array.ofDim(mm + 1)
  val bestTwoDiff: Array[Int] = Array.ofDim(n + 1)
  var cost, stage, v, v1, v2, f1, f2, sd1, lst, pr, nx, p, h, h1, r, delta, r2, min, i, j, len = 0
  var np1, larsel1, larsel2, shdw, w, ll, linem, la, lb, l, minrow, mincol, l1, l2, rcst, lmin, sd2, sd = 0
  var nLeft, m = n
  var done = false
  val inf = Int.MaxValue
  val unvis = 0

  def printAll() {
    if (verbose) {
      println("==================================")
      println("n: " + n + "\tm: " + m + "\tstage: " + stage + "\tv: " + v)
      println("len:\t" + len + "\tdone: " + done + "\tcost: " + cost)
      println("label:\t" + label.mkString(","))
      println("stack:\t" + stack.mkString(","))
      println("parent:\t" + parent.mkString(","))
      println("arsel1:\t" + arsel1.mkString(","))
      println("arsel2:\t" + arsel2.mkString(","))
      println("line:\t" + line.mkString(","))
      println("shadow:\t" + shadow.mkString(","))
      println("lnext:\t" + lnext.mkString(","))
      println("lpred:\t" + lpred.mkString(","))
      println("pred:\t" + pred.mkString(","))
    }
  }

  var k = 0
  var mainLoop = false

  def arbred(oC: Array[Array[ReversibleInt]], max: Int): Int = {
    if (verbose) println("arbred()")
    //Intitialization

    var k1 = 1
    while (k1 <= n) {
      k2 = 1
      while (k2 <= n) {
        c(k1)(k2) = oC(k1 - 1)(k2 - 1).value
        rc(k1)(k2) = c(k1)(k2)
        k2 += 1
      }
      k1 += 1
    }
    k = 1
    while (k <= n) {
      label(k) = unvis
      parent(k) = mm
      rc(k)(k) = inf
      k += 1
    }
    init()
    printAll()

    nLeft = n
    m = n
    cost = 0
    len = 0
    stage = n
    label(root) = root
    insert(root)
    printAll()

    var loop1 = true
    do {
      var loop2 = true
      do {
        if (!mainLoop) {
          //20
          newst()
          printAll()
          if (done) {
            loop2 = false
          }
        }
        if (loop2) {
          var loop3 = true
          while (loop3) {
            //30
            if (!mainLoop) {
              v = stack(len)
              minarc()
              if (min > max) return inf
            }
            mainLoop = false
            //40
            label(v) = stage
            arsel1(v) = i
            arsel2(v) = j
            u(v) = min
            cost = cost + min

            if (label(v1) != unvis) {
              loop3 = false
            }
            if (loop3) {
              insert(v1)
            }
            printAll
          }
        }

      } while (loop2 && label(v1) != unvis && label(v1) != stage)
      if (done) {
        loop1 = false
      }
      if (loop1) shrink()
      if (nLeft == 1) {
        loop1 = false
      }
      if (loop1) {
        v = m
        mainLoop = true
      }
    } while (loop1 && !done)

    //60
    arcarb()

    //computeTwoBestDiff()
    if (rcflag) {
      reduce()
    }
    k1 = 1
    while (k1 <= n) {
      k2 = 1
      while (k2 <= n) {
        rc(k1 - 1)(k2 - 1) = rc(k1)(k2)
        improvedRC(k1-1)(k2-1) = rc(k1)(k2)
        k2 += 1
      }
      k1 += 1
    }

    return cost
  }

  def arcarb() {
    if (verbose) println("arcarb()")
    k = 1
    while (k <= m) {
      delet(k) = false
      k += 1
    }
    pred(root) = 0
    delet(root) = true
    k = m
    do {
      if (!delet(k)) {
        j = arsel2(k)
        pred(j) = arsel1(k)
        while (j != k) {
          delet(j) = true
          j = parent(j)
        }
      }
      k -= 1
    } while (k > 0)
  }

  def init() {
    if (verbose) println("init()")
    k = 1
    while (k <= n) {
      line(k) = k
      sv(k) = k
      shadow(k) = 0
      lnext(k) = k + 1
      lpred(k + 1) = k
      k = k + 1
    }
    lnext(n + 1) = 1
    lpred(1) = n + 1
  }

  def insert(v: Int) {
    if (verbose) println("insert()" + v)
    len = len + 1
    stack(len) = v
    np1 = n + 1
    lst = lpred(np1)
    if (v == lst) {
      return
    }
    pr = lpred(v)
    nx = lnext(v)
    lnext(pr) = nx
    lpred(nx) = pr
    lnext(lst) = v
    lpred(v) = lst
    lnext(v) = np1
    lpred(np1) = v
  }

  def minarc() {
    if (verbose) println("minarc()" + v)
    j = v
    l2 = line(v)
    np1 = n + 1
    l1 = lnext(np1)
    min = rc(l1)(l2)

    l = lnext(l1)
    do {
      if (rc(l)(l2) < min) {
        min = rc(l)(l2)
        l1 = l
      }
      l = lnext(l)
    } while (l != np1)
    v1 = sv(l1)
    sd1 = shadow(l1)
    i = l1
    if (sd1 > 0) {
      i = rc(sd1)(l2)
    }
  }

  def newst() {
    if (verbose) println("newst()")
    done = true
    while (label(stage) > 0) {
      stage = stage - 1
      if (stage <= 0) {
        return
      }
    }

    done = false
    if (len != 0) {
      v1 = stack(1)
      l1 = line(v1)
      v2 = stack(len)
      l2 = line(v2)
      np1 = n + 1
      f1 = lnext(np1)
      f2 = lpred(l1)
      lnext(f2) = np1
      lpred(np1) = f2
      lnext(np1) = l1
      lpred(l1) = np1
      lnext(l2) = f1
      lpred(f1) = l2
    }
    len = 1
    stack(len) = stage
  }

  def reduce() {
    if (verbose) println("reduce()")
    np1 = n + 1
    v = np1
    while (v <= m) {
      fson(v) = 0
      v += 1
    }
    fson(mm) = 0
    v = 1
    while (v <= m) {
      p = parent(v)
      if (fson(p) == 0) {
        fson(p) = v
        broth(v) = 0
      } else {
        broth(v) = fson(p)
        fson(p) = v
      }
      v += 1
    }
    broth(mm) = mm

    h = 0
    h1 = h + 1
    v = mm
    do {
      while (v > n) {
        left(v) = h1
        v = fson(v)
      }
      h = h1
      h1 += 1
      node(h) = v
      while (broth(v) == 0) {
        v = parent(v)
        right(v) = h
      }
      v = broth(v)
    } while (v != mm)
    u(root) = 0

    h = 1
    while (h <= n) {
      j = node(h)
      if (j == root) {
        i = 1
        while (i <= n) {
          rc(i)(j) = c(i)(j)
          i += 1
        }
      } else {
        rc(j)(j) = c(j)(j) //90
        l = h - 1
        r = h + 1
        delta = u(j)
        p = parent(j)
        var loop = true
        while (loop) {
          l1 = left(p) //100
          while (l >= l1) {
            i = node(l)
            rc(i)(j) = c(i)(j) - delta
            l -= 1
          }
          r2 = right(p) //120
          while (r <= r2) {
            i = node(r)
            rc(i)(j) = c(i)(j) - delta
            r += 1
          }
          if (p == mm) {
            loop = false
          }
          if (loop) {
            delta = delta + u(p)
            p = parent(p)
          }
        }
      }

      h += 1
    }
  }

  def shrink() {
    if (verbose) println("shrink()")
    if (len == nLeft && stack(0) == v1) {
      nLeft = 1
      return
    }
    m = m + 1
    parent(m) = mm
    v = stack(len)
    larsel2 = line(v)
    do {
      w = stack(len)
      len -= 1
      parent(w) = m
      nLeft -= 1
      ll = line(w)
      reduc(ll) = u(w)

    } while (w != v1)
    larsel1 = line(v1)

    linem = larsel1
    shdw = larsel2
    np1 = n + 1
    la = lnext(np1)
    lb = lpred(larsel1)
    min = inf
    l = la

    var loop = true
    while (loop) {
      //30
      ll = larsel1
      minrow = rc(l)(ll) - reduc(ll)
      l1 = ll
      mincol = rc(ll)(l)
      l2 = ll
      //40
      do {
        ll = lnext(ll)
        rcst = rc(l)(ll) - reduc(ll)
        if (rcst < minrow) {
          minrow = rcst
          l1 = ll
        }
        if (rc(ll)(l) < mincol) {
          mincol = rc(ll)(l)
          l2 = ll
        }
      } while (ll != larsel2)
      //61
      if (minrow < min) {
        min = minrow
        lmin = l
      }
      //70
      rc(l)(linem) = minrow
      rc(l)(shdw) = l1
      sd1 = shadow(l1)
      if (sd1 > 0) {
        rc(l)(shdw) = rc(l)(sd1)
      }
      rc(linem)(l) = mincol
      rc(shdw)(l) = l2
      sd2 = shadow(l2)
      if (sd2 > 0) {
        rc(shdw)(l) = rc(sd2)(l)
      }
      sd = shadow(l)
      if (sd > 0) {
        rc(sd)(linem) = rc(sd)(l1)
        rc(linem)(sd) = rc(l2)(sd)
      }
      //80
      if (l == lb) {
        loop = false
      }
      if (loop) {
        l = lnext(l)
      }
    }

    line(m) = linem
    sv(linem) = m
    shadow(linem) = shdw
    rc(linem)(linem) = inf
    lnext(linem) = np1
    lpred(np1) = linem
    len += 1
    stack(len) = m
    nLeft += 1
    v1 = sv(lmin)
    j = rc(lmin)(shdw)
    sd = shadow(lmin)
    i = lmin
    if (sd > 0) {
      i = rc(sd)(linem)
    }
  }

  /*
   * Our improved rc
   */
  val improvedRC = Array.fill(n + 1, n + 1)(0)
  val bestDiffFromSrcUntilDest = Array.fill(n + 1, n + 1)(inf)
  val hasAtmostOneParent = Array.fill(n + 1)(false)

  def computeImprovedRC() {

    k1 = 1
    while (k1 <= n) {
      val pk1 = parent(k1)
      if (pk1 == mm || parent(pk1) == mm) hasAtmostOneParent(k1) = true
      k1 += 1
    }

    computeBestTwoDiff()

    /////////BestDiffBetween for v with at most one parent
    k1 = 1
    while (k1 <= n) {
      val pk1 = parent(k1)
      if (hasAtmostOneParent(k1)) {
        min = bestTwoDiff(k1) - u(pk1)
        k2 = pred(k1)
        while (min > 0 && hasAtmostOneParent(k2) && k2 != root) {
          bestDiffFromSrcUntilDest(k1)(k2) = min
          val currentBestDiff = bestTwoDiff(k2) - u(parent(k2))
          if (currentBestDiff < min) {
            min = currentBestDiff
          }
          k2 = pred(k2)
        }
      }
      k1 += 1
    }
    /////////////////////////////////

    ////////////// ImprovedRC
    k1 = 0
    while (k1 < n) {
      if (hasAtmostOneParent(k1+1)) {
        k2 = 0
        while (k2 < n) {
          val additionalCost = if (bestDiffFromSrcUntilDest(k1 + 1)(k2 + 1) == inf) 0 else bestDiffFromSrcUntilDest(k1 + 1)(k2 + 1)
          improvedRC(k1)(k2) += additionalCost
          bestDiffFromSrcUntilDest(k1 + 1)(k2 + 1) = inf
          k2 += 1
        }
      }
      hasAtmostOneParent(k1 + 1) = false
      k1 += 1
    }
  }

  def computeBestTwoDiff(): Unit = {
    /////////bestTwoDiff for each v in V
    k1 = 1
    while (k1 <= n) {
      if (hasAtmostOneParent(k1)) {
        var min1 = c(1)(k1)
        var min2 = inf
        k2 = 2
        while (k2 <= n) {
          val cK2K1 = c(k2)(k1)
          if (cK2K1 < min1) {
            val tmp = min1
            min1 = cK2K1
            min2 = tmp
          } else if (cK2K1 < min2) {
            min2 = cK2K1
          }
          k2 += 1
          bestTwoDiff(k1) = min2 - min1
        }
      }
      k1 += 1
    }
  }

}