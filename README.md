# CP4CAP repository #

**CP4CAP:** Constraint Programming for Constrained Arborescence Problem

This repository contains all source codes for the CP models, global constraints (StockingCost and IDStockingCost constraints) and the instances used for our experiments in the paper: [The MinArborescence Constraint](Link URL).

The implementations and tests have been realized within the [OscaR open source solver](https://bitbucket.org/oscarlib/oscar/wiki/Home).

### Usage ###

To test the different models as described in the [MinArborescence](Link URL) paper
(assuming that sbt and scala are well installed), 
please follow these instructions:

```
#!sh
    Clone the current repository : hg clone https://bitbucket.org/ratheilesse/cp4cap/
    Move into the clone's directory
    sbt
    update
    compile
    run <File> -t <timeout> 
```

in which:

```
#!sh

 <File>
        the input instance. It can be "data/RMWA/RMWA_50/i" with i in [1,...,100].
 -t <timeout> 
        Optional. The timeout (in seconds) to record the tree by the baseline model. 
        By default timeout = 30.
```

To test each model individually with a cost ordering search, use the "-m" option:


```
#!sh

run <File> -t <timeout> -m <model>

```

with

```
#!sh

-m <model>
Optional. 1 => Arbo model (decomposition). 2 => Arbo+LB model. 3. Arbo+MinArbo with LP reduced costs model. 4 => Arbo+MinArbo with improved reduced costs.


```

To work on the source code with [IntelliJ Idea] or [eclipse]:

```
#!sh
    Download and install the latest version of IntelliJ Idea or eclipse
    Move into the clone's directory
    sbt 
    update
    compile
    run
    gen-idea // for IntelliJ Idea
    eclipse // for Eclipse
    import project in your IDE and run it
```


### Questions/Comments ###
For any question/comment, feel free to write to [ratheilesse@gmail.com](Link URL) or [pschaus@gmail.com](Link URL)
